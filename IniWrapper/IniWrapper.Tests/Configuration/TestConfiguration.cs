﻿using System.Collections.Generic;

namespace IniWrapper.Tests.Configuration
{
    public class TestConfiguration
    {
        public string TestString { get; set; }
        public List<int> TestIntList { get; set; }
    }
}